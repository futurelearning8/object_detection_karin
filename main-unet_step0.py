from dataset import get_training_validation_generators, get_testing_generators
from models import unet_model, unet_model_phaze2, loss_return_0, CustomLossStep3,unet_model_phaze2_deeper,single_step_model_full
import tensorflow as tf
import datetime
from params import EPOCHS, LEARNING_RATE,BATCH_SIZE, LOAD,BOTH,SEG
from numpy.random import seed
from tensorflow import keras
seed(42)  # keras seed fixing import tensorflow as tf
tf.random.set_seed(42)  # tensorflow seed fixing


def main():
    # Generators - training and validation
    training_generator, validation_generator = get_training_validation_generators(0.9)
    # Generators - testing
    testing_generator = get_testing_generators()

    if not LOAD:
        print("Creating a model...")
        # Model Creation
        # model for centers detection
        #unet = unet_model_phaze2()
        #unet = unet_model_phaze2_deeper()
        unet = single_step_model_full()

    else:
        # Model Loading
        model_path = 'models/1006-0000lr_0.0005batch_size_32/validation.07.h5'

        print(f"Load model: {model_path}...")
        # Best Model Loading
        unet = keras.models.load_model(model_path, compile=False)

    tf.keras.utils.plot_model(unet, to_file="Model detection only.png", show_shapes=True)
    if not BOTH:
        for layer in [l for l in unet.layers if 'seg' in l.name]:
            layer.trainable = False
        for layer in [l for l in unet.layers if 'seg' not in l.name]:
            layer.trainable = True
    else:
        for layer in unet.layers :
            layer.trainable = True

    print(unet.summary())
    tf.keras.utils.plot_model(unet, to_file="Unet_depper_2.png", show_shapes=True)

    for lr in LEARNING_RATE:
        # model compile
        opt = tf.keras.optimizers.Adam(learning_rate=lr)

        if  BOTH:
            seg_loss = "binary_crossentropy"
        else:
            seg_loss = loss_return_0
        if SEG:
            det_loss = loss_return_0
        else:
            det_loss = CustomLossStep3
        losses = {
            "detection_output": det_loss,
            "segmentation_output": seg_loss,
        }
        lossweights = {"detection_output": 1, "segmentation_output": 1}
        unet.compile(optimizer=opt, loss=losses,loss_weights=lossweights ,metrics=["accuracy"])

        # callbackss
        log_dir = datetime.datetime.now().strftime("%d%m-%H%M")
        filepath = 'models/' + log_dir + 'lr_' + str(lr) + 'batch_size_' + str(BATCH_SIZE) #+ 'aug_is' + str(AUG)
        filepath_v = 'models/' + log_dir + 'lr_' + str(lr) + 'batch_size_' + str(BATCH_SIZE)

        checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(filepath=filepath + '/train.{epoch:02d}.h5',
                                                                 monitor='loss',
                                                                 verbose=1, save_best_only=True, mode='min')
        checkpoint_callback_val = tf.keras.callbacks.ModelCheckpoint(filepath=filepath_v + '/validation.{epoch:02d}.h5',
                                                                     monitor='val_loss',
                                                                     verbose=1, save_best_only=True, mode='min')

        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir="logs/fit/" + log_dir, histogram_freq=1)
        early_stop_callback = tf.keras.callbacks.EarlyStopping(patience=10, monitor='loss')  # lossval_loss
        reduce_lr_on_plateau_callback = tf.keras.callbacks.ReduceLROnPlateau(monitor="loss", factor=0.5, patience=10,
                                                                             verbose=1, mode="min", min_delta=0.0001,
                                                                             cooldown=0, min_lr=0)
        callbacks_list = [checkpoint_callback, tensorboard_callback, reduce_lr_on_plateau_callback, checkpoint_callback_val]

        if not LOAD:
            # Train model on dataset
            unet.fit(x=training_generator,
                     validation_data=validation_generator,
                     epochs=EPOCHS,
                     callbacks=callbacks_list,
                     #steps_per_epoch=250
                     )#            validation_data=validation_generator,, validation_steps=2
        else:
            unet.fit(x=training_generator,
                     validation_data=validation_generator,
                     epochs=EPOCHS,
                     callbacks=callbacks_list,
                     initial_epoch=7)  # validation_data=validation_generator, steps_per_epoch = 500, validation_steps=2

    results = unet.evaluate(testing_generator)
    print(f"results on testing: {results}")
    results = unet.evaluate(validation_generator)
    print(f"results on validation: {results}")
    results = unet.evaluate(training_generator)
    print(f"results on training: {results}")


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

