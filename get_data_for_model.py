from pycocotools.coco import COCO
import matplotlib.pyplot as plt
import pylab
import numpy as np
pylab.rcParams['figure.figsize'] = (8.0, 10.0)


def get_data_for_model(annFile, min_area=10000, max_area=100000, display=False):
    coco = COCO(annFile)
    # get all images containing given categories, select one at random
    person_cat_id = coco.getCatIds(catNms=['person']) #,'dog','skateboard'
    person_images_id = coco.getImgIds(catIds=person_cat_id)

    info = []
    for person_im_id in person_images_id:
        bboxes = []
        annIds = coco.getAnnIds(person_im_id, catIds=person_cat_id, areaRng=[min_area, max_area])
        if annIds:
            person_anns = coco.loadAnns(ids=annIds)

            ann_0 = person_anns[0]
            # human_mask = np.zeros((human_image["height"], human_image["width"]))  # coco.annToMask(ann_0)
            # image_id
            image_id = ann_0["image_id"]
            # file name, width, height
            human_image = coco.loadImgs(image_id)[0]
            human_mask = np.zeros((human_image["height"], human_image["width"]), dtype=np.int8)  # coco.annToMask(ann_0)

            for i, im_anns in enumerate(person_anns):
                # mask
                human_mask += (i+1) * coco.annToMask(im_anns)
                # bbox
                bbox = im_anns["bbox"]
                bboxes.append(bbox)

            instance_mask = human_mask.copy()
            human_mask[human_mask > 1] = 1

            if display:
                plt.imshow(instance_mask)
                plt.show()
                plt.imshow(human_mask)
                plt.show()

            info.append([human_image["file_name"], bboxes, human_mask, image_id, (human_image["width"],
                                                                                  human_image["height"]),
                         instance_mask])

    return info

# # Test code
# dataDir = '../data/coco'
# dataType = 'val2017'
# annFile = '{}/annotations/instances_{}.json'.format(dataDir, dataType)
# info = get_data_for_model(annFile, display=False)
# print(len(info))
