import os
import albumentations as A
import numpy as np
import pandas as pd
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.utils import Sequence
from params import SEG
from tensorflow.keras.applications import EfficientNetB0
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D
from tensorflow.keras.models import Sequential


#datagen =  DataGeneratorBBox(image_bbox_lst_training, mode="Training", **params)

class AugmentDataGenerator(Sequence):
    def __init__(self, datagen, augment=None):
        self.datagen =  datagen
        if augment is None:
            self.augment = A.Compose([])
        else:
            self.augment = augment

    def __len__(self):
        return len(self.datagen)

    def __getitem__(self, x):
        augmented_x = []
        augmented_y = []
        (images, y_trues) = self.datagen[x]

        if SEG:
            for image, y_true in zip(images, y_trues):
                transformed = self.augment(image=image, y_true=y_true)
                augmented_x.append(transformed['image'])
                augmented_y.append(transformed['y_true'])
        else:
            for image in images:
                transformed = self.augment(image=np.uint8(image*255))
                augmented_x.append(transformed['image']/255.0)

        if SEG:
            return np.array(augmented_x), np.array(augmented_y)
        else:
            return np.array(augmented_x), y_trues

    def getitem_id(self, x):
        augmented_x = []
        augmented_y = []
        X, bboxes, masks, ids, wh, instance_masks = self.datagen.getitem_id(x)
        # (images, y_trues) = self.datagen.getitem_id(x)

        if SEG:
            for image, y_true in zip(X, masks):
                transformed = self.augment(image=image, y_true=y_true)
                augmented_x.append(transformed['image'])
                augmented_y.append(transformed['y_true'])
        else:
            for image in X:
                transformed = self.augment(image=np.uint8(image*255))
                augmented_x.append(transformed['image']/255.0)

        if SEG:
            return np.array(augmented_x), bboxes, np.array(augmented_y), ids, wh
        else:
            return np.array(augmented_x), bboxes, masks, ids, wh, instance_masks



    """
    def __getitem__(self, x):
        images, *rest = self.datagen[x]
        augmented = []
        for image in images:
            image = self.augment(image=image)['image']
            augmented.append(image)
        return (np.array(augmented), *rest)
    """

# model.fit(datagen, epochs=1, workers=4)