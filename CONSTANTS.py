DATA_PATH = "../data/coco/"
ANNOTATIONS_PATH = DATA_PATH + 'annotations/'
INSTANCES_TRAIN_PATH = ANNOTATIONS_PATH + 'instances_train2017.json'
INSTANCES_TEST_PATH = ANNOTATIONS_PATH + 'instances_val2017.json'
IMAGES_PATH = DATA_PATH + 'images/'
TRAIN_PATH = IMAGES_PATH + 'train2017/'
TESTING_PATH = IMAGES_PATH + 'val2017/'
PATH_TO_RESULT = 'results_detections.json'
PATH_TO_SEG_JSON = 'results_segmentations.json'


