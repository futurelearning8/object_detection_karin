from tensorflow import keras
from dataset import get_testing_generators, get_training_validation_generators
import tensorflow as tf
from models import CustomLossStep3
from numpy.random import seed
from object_detection_utils import NMS_on_generator
from CONSTANTS import PATH_TO_RESULT, INSTANCES_TEST_PATH
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import numpy as np
from params import LEARNING_RATE

seed(42)  # keras seed fixing import tensorflow as tf
tf.random.set_seed(42)  # tensorflow seed fixing


def evaluate_model():
    # model_path = 'models/model-train.76.h5'
    model_path = 'models/model-validation test.02.h5'
    # Best Model Loading
    ssm = keras.models.load_model(model_path, compile=False)

    # compile best model with relevant optimizer, loss and metrics
    opt = tf.keras.optimizers.Adam(learning_rate=LEARNING_RATE)

    # Bug with accuracy
    ssm.compile(optimizer=opt, loss=CustomLossStep3, metrics=['accuracy'])

    # Generators - training and validation
    training_generator, validation_generator = get_training_validation_generators(0.9)
    # Generators - testing
    testing_generator = get_testing_generators()

    # # Results for test set
    # results = ssm.evaluate(testing_generator)
    # print(results)

    # https://github.com/cocodataset/cocoapi/blob/master/PythonAPI/pycocoEvalDemo.ipynb

    annFile = INSTANCES_TEST_PATH
    cocoGt = COCO(annFile)
    annType = ['segm', 'bbox', 'keypoints']
    annType = annType[1]

    MAP_MAR = []
    for th in np.arange(0.8, 0.9, 0.1):
        for nms in np.arange(0.6, 0.7, 0.1):
            lst_ids = NMS_on_generator(ssm, testing_generator, th, nms, display=False)

            cocoDt = cocoGt.loadRes(PATH_TO_RESULT)

            # running evaluation
            cocoEval = COCOeval(cocoGt, cocoDt, annType)
            # imgIds = sorted(cocoGt.getImgIds())
            cocoEval.params.imgIds = sorted(lst_ids)
            cocoEval.params.catIds = [1]
            cocoEval.params.areaRng = [[0, 1e10], [0, 1e4], [1e4, 1e5], [1e5, 1e10]]
            cocoEval.evaluate()
            cocoEval.accumulate()
            cocoEval.summarize()
            stats = cocoEval.stats
            MAP_MAR.append((stats[4], stats[10]))
            print(f"th={th}, nmsTh={nms}, res:\n {stats}\n")
    print(f"map:{MAP_MAR}\n")


if __name__ == "__main__":
    evaluate_model()
