import numpy as np
import json
from params import THRESHOLD, BATCH_SIZE, HEIGHT, WIDTH, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, NMS_THRESHOLD
from CONSTANTS import PATH_TO_RESULT, PATH_TO_SEG_JSON
from utils import show_image_gt_pred, display_image
import cv2
from pycocotools.mask import encode


def NMS_on_generator(ssm, generator, th, nms_thresh, display=False):
    lst_ids = []
    lst_rois = []
    lst_scores = []
    lst_masks = []
    wh_lst = []
    for i in range(generator.__len__()):
        X, bboxes, masks, ids, wh, instance_masks = generator.getitem_id(i)
        wh_lst += wh
        lst_ids.append(ids)
        for j in range(BATCH_SIZE):
            x = X[j].reshape((1, HEIGHT, WIDTH, 3))
            y = bboxes[j].reshape((1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 5))
            # mask = masks[j].reshape((1, HEIGHT, HEIGHT, 1))
            mask = masks[j].reshape((HEIGHT, HEIGHT))
            instance_mask = instance_masks[j].reshape((HEIGHT, HEIGHT))
            predicted_bbox, predicted_mask = ssm.predict(x)
            predicted_mask[predicted_mask > 0.5] = 1
            predicted_mask[predicted_mask < 0.5] = 0
            # print(f"y:\n{y[..., 0].reshape(LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH)}"
            #       f"\npredicted:\n{np.around(predicted_bbox[..., 0].reshape(LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH), 3)}")

            #### y
            matrix_score_y = y[..., 0].reshape(LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1)
            mask_indices_y, score_y = score_matrix_to_lst(matrix_score_y, th)

            # return bboxes according mask - X, bboxes, W, H
            matrices_bbox_y = y[..., 1:5].reshape(LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 4)
            rois_y = bboxes_matries_to_lst_org_size_xywh(mask_indices_y, matrices_bbox_y, wh[j])

            #### predicted_bbox
            matrix_score = predicted_bbox[..., 0].reshape(LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1)
            mask_indices, score = score_matrix_to_lst(matrix_score, th)

            # return bboxes according mask - X, bboxes, W, H
            matrices_bbox = predicted_bbox[..., 1:5].reshape(LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 4)
            rois_before = bboxes_matries_to_lst_org_size_xywh(mask_indices, matrices_bbox, wh[j])

            # Run NMS to delete un-relevant bboxes
            rois_after_y_hat, score_y_hat, _ = NMS(rois_before, score, nms_thresh)

            # masks according W, H
            mask_wh = mask_to_org_size_xywh(predicted_mask.reshape((HEIGHT, WIDTH)), wh[j])
            if display:
                _, _, img_true_centers_indexed, img_predicted_centers_indexed = show_image_gt_pred(x, y, predicted_bbox)
                rois_y_disp = bboxes_matries_to_lst_display_size_xywh(mask_indices_y, matrices_bbox_y)
                rois_before_disp = bboxes_matries_to_lst_display_size_xywh(mask_indices, matrices_bbox)
                rois_after_y_hat_disp, _, relevant_indices = NMS(rois_before_disp, score, nms_thresh)
                instance_seg_mask = get_instance_mask_from_bbox_and_semantic(predicted_mask.reshape((HEIGHT, WIDTH)),
                                                                             rois_after_y_hat_disp)
                display_image(I=x.reshape((HEIGHT, WIDTH, 3)),
                              mask=instance_mask.reshape((HEIGHT, WIDTH)),
                              predicted_mask=predicted_mask.reshape((HEIGHT, WIDTH)),
                              predicted_instance_mask=instance_seg_mask,
                              centers=img_true_centers_indexed,
                              predicted_centers=[img_predicted_centers_indexed[r_i] for r_i in relevant_indices],
                              bboxes=rois_y_disp,
                              predicted_bboxes=rois_after_y_hat_disp)

            lst_rois.append(rois_after_y_hat)
            lst_scores.append(score_y_hat)
            lst_masks.append(mask_wh)

    build_coco_det_results(np.array(lst_ids).flatten(), lst_rois, lst_scores, lst_masks, wh_lst)
    build_coco_seg_results(np.array(lst_ids).flatten(), lst_masks, lst_scores)
    return np.array(lst_ids).flatten()


def get_instance_mask_from_bbox_and_semantic(mask, bboxes):
    instance_mask = np.zeros(mask.shape)
    for i in range(bboxes.shape[0]):
        bbox_mask = bbox_to_mask(bboxes[i], (mask.shape[1], mask.shape[0]))
        instance = np.logical_and(mask, bbox_mask)
        instance_mask += (i + 1) * instance
    return instance_mask


def bbox_to_mask(bbox, wh):
    bbox_mask = np.zeros((wh[1], wh[0]), dtype=np.int8)
    x, y, w, h = bbox
    bbox_mask[int(y):int(y+h), int(x):int(x+w)] = 1
    return bbox_mask


def bboxes_matries_to_lst_display_size_xywh(mask_indices, matrices):
    # indices in resized image
    width_label_scale = WIDTH / LAST_LAYER_WIDTH
    height_label_scale = HEIGHT / LAST_LAYER_HEIGHT

    rois = [
        (max((x + matrices[y, x, 0]) * width_label_scale - matrices[y, x, 2] / 2 * WIDTH, 0),
         max((y + matrices[y, x, 1]) * height_label_scale - matrices[y, x, 3] / 2 * HEIGHT, 0),
         matrices[y, x, 2] * WIDTH,
         matrices[y, x, 3] * HEIGHT)
        for (y, x) in mask_indices()]
    return np.array(rois)


def bboxes_matries_to_lst_org_size_xywh(mask_indices, matrices, wh):
    # indices in resized image
    width_label_scale = wh[0] / LAST_LAYER_WIDTH
    height_label_scale = wh[1] / LAST_LAYER_HEIGHT

    rois = [
        (max((x + matrices[y, x, 0]) * width_label_scale - matrices[y, x, 2] / 2 * wh[0], 0),
         max((y + matrices[y, x, 1]) * height_label_scale - matrices[y, x, 3] / 2 * wh[1], 0),
         matrices[y, x, 2] * wh[0],
         matrices[y, x, 3] * wh[1])
        for (y, x) in mask_indices()]
    return np.array(rois)


def mask_to_org_size_xywh(mask, wh):
    # indices in resized image
    resized_mask = cv2.resize(mask, wh)
    return resized_mask


def score_matrix_to_lst(matrix_score, threshold=THRESHOLD):
    # get score on relevant boxes
    mask_indices =  lambda: zip(*np.where(matrix_score[:, :, 0] > threshold))
    score = [matrix_score[index] for index in mask_indices()]
    return mask_indices, np.array(score).flatten()


def build_coco_det_results(image_ids, rois, scores, masks, whs):
    """
    https://github.com/cocodataset/cocoapi/issues/343
    https://cocodataset.org/#format-results
    """
    results = []
    for image_id, roi, score, mask, wh in zip(image_ids, rois, scores, masks, whs):
        # Loop through detections
        for i in range(roi.shape[0]):
            bbox_score = float("{:.2f}".format(score[i]))
            bbox_mask = bbox_to_mask(roi[i], wh)
            bbox = np.around(roi[i], 2)
            seg = np.logical_and(mask, bbox_mask)
            rle = encode(np.asfortranarray(seg.astype('uint8')))
            rle['counts'] = str(rle['counts'], "utf-8")

            result = {
                "image_id": int(image_id),
                "category_id": 1,
                "bbox": [float(bbox[0]), float(bbox[1]), float(bbox[2]), float(bbox[3])],
                "score": bbox_score,
                "segmentation": rle
            }
            results.append(result)
    with open(PATH_TO_RESULT, 'w') as json_file:
        json.dump(results, json_file)
    return results


def build_coco_seg_results(image_ids, masks, scores):
    """
    https://github.com/cocodataset/cocoapi/issues/343
    https://cocodataset.org/#format-results
    """
    results = []
    for image_id, mask, score in zip(image_ids, masks, scores):
        rle = encode(np.asfortranarray(mask.astype('uint8')))
        rle['counts'] = str(rle['counts'], "utf-8")
        # Loop through detections
        # for rle in range(rle_lst):
        #     # bbox_score = float("{:.2f}".format(score[i]))
        #     #bbox = np.around(roi[i], 2)

        result = {
            "image_id": int(image_id),
            "category_id": 1,
            "segmentation": rle,
            "score": 1  # bbox_score
        }
        results.append(result)
    with open(PATH_TO_SEG_JSON, 'w') as json_file:
        json.dump(results, json_file)
    return results


def NMS(bboxes, scores, thresh=NMS_THRESHOLD):
    n_bbox = bboxes.shape[0]
    bboxes_tlbr = xywh_to_tlbr_area(bboxes)
    order = scores.argsort()[::-1]
    suppressed = np.zeros(n_bbox)

    for _i in range(n_bbox):
        # by score
        i = order[_i]
        if suppressed[i] == 1:
            continue
        i_x1, i_y1, i_x2, i_y2, i_area = bboxes_tlbr[i]
        for _j in range(_i + 1, n_bbox):
            j = order[_j]
            if suppressed[j] == 1:
                continue
            xx1 = max(i_x1, bboxes_tlbr[j][0])
            yy1 = max(i_y1, bboxes_tlbr[j][1])
            xx2 = min(i_x2, bboxes_tlbr[j][2])
            yy2 = min(i_y2, bboxes_tlbr[j][3])

            intersection_width = max(xx2 - xx1, 0)
            intersection_height = max(yy2 - yy1, 0)
            inter = intersection_width * intersection_height
            ovr = inter / (i_area + bboxes_tlbr[j][4] - inter)
            if ovr >= thresh:
                suppressed[j] = 1
    relevant_indeces = np.where(suppressed == 0)[0]
    return bboxes[relevant_indeces], scores[relevant_indeces], relevant_indeces


def xywh_to_tlbr_area(bboxes):
    tlbr_lst = []
    for bbox in bboxes:
        i_x1 = bbox[0]
        i_y1 = bbox[1]
        i_w = bbox[2]
        i_h = bbox[3]
        i_x2 = i_x1 + i_w
        i_y2 = i_y1 + i_h
        tlbr_lst.append((i_x1, i_y1, i_x2, i_y2, i_w*i_h))
    return tlbr_lst
