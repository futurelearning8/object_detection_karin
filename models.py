from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import Input, Conv2D, MaxPool2D, concatenate,UpSampling2D,Dropout,MaxPooling2D,BatchNormalization

from tensorflow.keras.models import Model
from params import HEIGHT, WIDTH
from params import *
import tensorflow as tf

SEG_LOSS_WEIGHT = 10
# HEAD_SIZE = 8


def unet_model():
    in1 = Input(shape=(RESIZE_X, RESIZE_Y, 3))

    conv1 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(in1)
    conv1 = Dropout(0.2)(conv1)
    conv1 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(conv1)
    pool1 = MaxPooling2D((2, 2))(conv1)

    conv2 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(pool1)
    conv2 = Dropout(0.2)(conv2)
    conv2 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(conv2)
    pool2 = MaxPooling2D((2, 2))(conv2)

    conv3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(pool2)
    conv3 = Dropout(0.2)(conv3)
    conv3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(conv3)
    pool3 = MaxPooling2D((2, 2))(conv3)

    conv4 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(pool3)
    conv4 = Dropout(0.2)(conv4)
    conv4 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(conv4)

    up1 = concatenate([UpSampling2D((2, 2))(conv4), conv3], axis=-1)
    conv5 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(up1)
    conv5 = Dropout(0.2)(conv5)
    conv5 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(conv5)

    up2 = concatenate([UpSampling2D((2, 2))(conv5), conv2], axis=-1)
    conv6 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(up2)
    conv6 = Dropout(0.2)(conv6)
    conv6 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(conv6)

    up2 = concatenate([UpSampling2D((2, 2))(conv6), conv1], axis=-1)
    conv7 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(up2)
    conv7 = Dropout(0.2)(conv7)
    conv7 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(conv7)
    segmentation = Conv2D(1, (1, 1), activation='sigmoid', name='seg')(conv7)

    model = Model(inputs=[in1], outputs=[segmentation])

    return model


def unet_model_phaze2():
    in1 = Input(shape=(RESIZE_Y, RESIZE_X, 3))

    conv1 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv1_1")(in1)
    conv1 = Dropout(0.2, name="share_dropout1")(conv1)
    conv1 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv1_2")(conv1)
    pool1 = MaxPooling2D((2, 2), name="share_pool1")(conv1)

    conv2 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv2_1")(pool1)
    conv2 = Dropout(0.2, name="share_dropout2")(conv2)
    conv2 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv2_2")(conv2)
    pool2 = MaxPooling2D((2, 2), name="share_pool2")(conv2)

    conv3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv3_1")(pool2)
    conv3 = Dropout(0.2, name="share_dropout3")(conv3)
    conv3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv3_2")(conv3)
    pool3 = MaxPooling2D((2, 2), name="share_pool3")(conv3)

    conv4 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv4_1")(pool3)
    conv4 = Dropout(0.2, name="share_dropout4")(conv4)
    conv4 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv4_2")(conv4)

    # here add heads im = 64x64x128 in case of 512x512 input
    #first go down to 8x8
    # todo if input is 192x192 then one less max pool to get to 6x6 enventually
    maxpool_head1 = MaxPooling2D((2, 2), name="det_pool1")(conv4)
    conv_head1 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="det_conv1")(maxpool_head1)
    maxpool_head2 = MaxPooling2D((2, 2), name="det_pool2")(conv_head1)
    conv_head2 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="det_conv2")(maxpool_head2)
    # maxpool_head3 =  MaxPooling2D((2, 2))(conv_head2)
    # conv_head3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(maxpool_head3)
    # conv_head3 = 8x8x128

    output_1 = Conv2D(filters=1, kernel_size=3, padding="same", activation="sigmoid", name="det_classification_conv")(conv_head2)
    output_2 = Conv2D(filters=4, kernel_size=3, padding="same", activation="relu", name="det_regression_conv")(conv_head2)
    #phaze1_output 128x128
    detection_output = concatenate([output_1, output_2], axis=3, name="detection_output")

    up1 = concatenate([UpSampling2D((2, 2), name="seg_us1")(conv4), conv3], axis=-1, name="seg_conc1")
    conv5 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv5_1")(up1)
    conv5 = Dropout(0.2, name="seg_dropout1")(conv5)
    conv5 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv5_2")(conv5)

    up2 = concatenate([UpSampling2D((2, 2), name="seg_us2")(conv5), conv2], axis=-1, name="seg_conc2")
    conv6 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv6_1")(up2)
    conv6 = Dropout(0.2, name="seg_dropout2")(conv6)
    conv6 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv6_2")(conv6)

    up2 = concatenate([UpSampling2D((2, 2), name="seg_us3")(conv6), conv1], axis=-1, name="seg_conc3")
    conv7 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv7_1")(up2)
    conv7 = Dropout(0.2, name="seg_dropout3")(conv7)
    conv7 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv7_2")(conv7)
    # segmentation = 512x512x1
    segmentation_output = Conv2D(1, (1, 1), activation='sigmoid', name="segmentation_output")(conv7)

    model = Model(inputs=[in1], outputs=[detection_output, segmentation_output])

    return model


def unet_model_phaze2_deeper():
    in1 = Input(shape=(RESIZE_Y, RESIZE_X, 3))

    conv1 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv1_1")(in1)
    conv1 = Dropout(0.2, name="share_dropout1")(conv1)
    conv1 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv1_2")(conv1)
    pool1 = MaxPooling2D((2, 2), name="share_pool1")(conv1)

    conv2 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv2_1")(pool1)
    conv2 = Dropout(0.2, name="share_dropout2")(conv2)
    conv2 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv2_2")(conv2)
    conv2 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv2_3")(conv2)

    pool2 = MaxPooling2D((2, 2), name="share_pool2")(conv2)

    conv3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv3_1")(pool2)
    conv3 = Dropout(0.2, name="share_dropout3")(conv3)
    conv3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv3_2")(conv3)
    conv3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv3_3")(conv3)
    conv3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv3_4")(conv3)
    pool3 = MaxPooling2D((2, 2), name="share_pool3")(conv3)

    conv4 = Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv4_1")(pool3)
    conv4 = Dropout(0.2, name="share_dropout4")(conv4)
    conv4 = Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv4_2")(conv4)
    conv4 = Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="share_conv4_3")(conv4)

    # here add heads im = 64x64x128 in case of 512x512 input
    #first go down to 8x8
    # todo if input is 192x192 then one less max pool to get to 6x6 enventually
    maxpool_head1 = MaxPooling2D((2, 2), name="det_pool1")(conv4)
    conv_head1 = Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="det_conv1")(maxpool_head1)
    maxpool_head2 = MaxPooling2D((2, 2), name="det_pool2")(conv_head1)
    conv_head2 = Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="det_conv2")(maxpool_head2)
    # maxpool_head3 =  MaxPooling2D((2, 2))(conv_head2)
    # conv_head3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(maxpool_head3)
    # conv_head3 = 8x8x128

    output_1 = Conv2D(filters=1, kernel_size=3, padding="same", activation="sigmoid", name="det_classification_conv")(conv_head2)
    output_2 = Conv2D(filters=4, kernel_size=3, padding="same", activation="relu", name="det_regression_conv")(conv_head2)
    #phaze1_output 128x128
    detection_output = concatenate([output_1, output_2], axis=3, name="detection_output")

    up1 = concatenate([UpSampling2D((2, 2), name="seg_us1")(conv4), conv3], axis=-1, name="seg_conc1")
    conv5 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv5_1")(up1)
    conv5 = Dropout(0.2, name="seg_dropout1")(conv5)
    conv5 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv5_2")(conv5)

    up2 = concatenate([UpSampling2D((2, 2), name="seg_us2")(conv5), conv2], axis=-1, name="seg_conc2")
    conv6 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv6_1")(up2)
    conv6 = Dropout(0.2, name="seg_dropout2")(conv6)
    conv6 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv6_2")(conv6)

    up2 = concatenate([UpSampling2D((2, 2), name="seg_us3")(conv6), conv1], axis=-1, name="seg_conc3")
    conv7 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv7_1")(up2)
    conv7 = Dropout(0.2, name="seg_dropout3")(conv7)
    conv7 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same', name="seg_conv7_2")(conv7)
    # segmentation = 512x512x1
    segmentation_output = Conv2D(1, (1, 1), activation='sigmoid', name="segmentation_output")(conv7)

    model = Model(inputs=[in1], outputs=[detection_output, segmentation_output])

    return model


def loss_return_0(y_true, y_pred):
    return 0


# def CustomLossSegmentation_phaze2(y_true, y_pred):
#
#     y_true = y_true[0]
#     y_true_seg = y_true[1]
#     y_pred = y_pred[0]
#     y_pred_seg = y_pred[1]
#
#     # 1. Classification
#     is_center = y_true[:, :, :, 0] == 1
#     center_error = 30 * keras.losses.binary_crossentropy(tf.reshape(y_true[:, :, :, 0], [-1, HEAD_SIZE, HEAD_SIZE, 1]), tf.reshape(y_pred[:, :, :, 0], [-1, HEAD_SIZE, HEAD_SIZE, 1]))
#     non_center_error = keras.losses.binary_crossentropy(tf.reshape(y_true[:, :, :, 0], [-1, HEAD_SIZE, HEAD_SIZE, 1]), tf.reshape(y_pred[:, :, :, 0], [-1, HEAD_SIZE, HEAD_SIZE, 1]))
#     # Returns the right loss for each type of error.
#     loss_classification = tf.where(is_center, center_error, non_center_error)
#
#     # 2. correction- x/y
#     los_corr_x_center = 30 * tf.keras.losses.MSE(tf.reshape(y_true[:, :, :, 1], [-1, HEAD_SIZE, HEAD_SIZE, 1]), tf.reshape(y_pred[:, :, :, 1], [-1, HEAD_SIZE, HEAD_SIZE, 1]))
#     loss_corr_y_center = 30 * tf.keras.losses.MSE(tf.reshape(y_true[:, :, :, 2], [-1, HEAD_SIZE, HEAD_SIZE, 1]), tf.reshape(y_pred[:, :, :, 2], [-1, HEAD_SIZE, HEAD_SIZE, 1]))
#     loss_regr_corr_x = tf.where(is_center, los_corr_x_center, 0)
#     loss_regr_corr_y = tf.where(is_center, loss_corr_y_center, 0)
#
#     # 3. bbox- width/height
#     los_width = 30 * tf.keras.losses.MSE(tf.reshape(y_true[:, :, :, 3], [-1, HEAD_SIZE, HEAD_SIZE, 1]), tf.reshape(y_pred[:, :, :, 3], [-1,HEAD_SIZE, HEAD_SIZE, 1]))
#     loss_height = 30 * tf.keras.losses.MSE(tf.reshape(y_true[:, :, :, 4], [-1, HEAD_SIZE, HEAD_SIZE, 1]), tf.reshape(y_pred[:, :, :, 4], [-1, HEAD_SIZE, HEAD_SIZE, 1]))
#     loss_regr_width = tf.where(is_center, los_width, 0)
#     loss_regr_height = tf.where(is_center, loss_height, 0)
#
#     # 3. classification weight
#     alpha = 0.4
#     beta = 0.3
#
#     # segmentation_loss = SEG_LOSS_WEIGHT * tf.keras.losses.binary_crossentropy(tf.reshape(y_true_seg, [-1, RESIZE_Y,RESIZE_X , 1]), tf.reshape(y_pred_seg, [-1,RESIZE_Y ,RESIZE_X , 1]))
#     loss = alpha * loss_classification + beta * (loss_regr_width + loss_regr_height) + \
#            (1 - alpha - beta) * (loss_regr_corr_x + loss_regr_corr_y)
#
#     return loss

#######################################################################################################

def block_layers(components, f, a, name="shared",max_pool=True,up_sample=False):
    for i in range(components):
        curr_name = name + "_conv_" + str(i)
    a = Conv2D(filters=f, kernel_size=3, padding="same", activation="relu", name=curr_name)(a)
    if i%2 ==0:
        a= BatchNormalization()(a)
    if max_pool:
        curr_name = name + "_maxpool"
        a = MaxPool2D(pool_size=(2, 2),name=curr_name)(a)
    if up_sample:
        curr_name = name + "_upsample"
        a = UpSampling2D((2, 2),name=curr_name)(a)



    return a

# todo dropout + BNg
def single_step_model_full():
    inputs = Input(shape=(RESIZE_Y, RESIZE_X, 3))
    conv1 = block_layers(2, 32, inputs,name="shared_1")
    conv2 = block_layers(3, 64, conv1,name="shared_2")
    conv2 = Dropout(0.2, name="share_dropout1")(conv2)

    conv3 = block_layers(4, 128, conv2,name="shared_3")
    conv4 = block_layers(3, 256, conv3,name="shared_4")
    conv5 = block_layers(3, 512, conv4,name='det1')
    conv5 = Dropout(0.2, name="share_dropout2")(conv5)
    conv6 = block_layers(3, 1024, conv5, name='det2',max_pool=False)
    output_1 = Conv2D(filters=1, kernel_size=3, padding="same", activation="sigmoid",name='det_conv_out1')(conv6)
    output_2 = Conv2D(filters=4, kernel_size=3, padding="same", activation="relu",name='det_conv_out2')(conv6)
    detection_output = concatenate([output_1, output_2], axis=3,name='detection_output')

    conv7 = block_layers(2, 1024, conv6,name='seg1', max_pool=False,up_sample=True)
    conv7 = concatenate([conv7, conv4], axis=-1, name="seg_conc1")
    conv8 = block_layers(3, 512, conv7, name='seg2',max_pool=False,up_sample=True)
    conv8 = Dropout(0.2, name="seg_dropout1")(conv8)
    conv8 = concatenate([conv8, conv3], axis=-1, name="seg_conc2")
    conv9 = block_layers(4, 256, conv8, name='seg3',max_pool=False,up_sample=True)
    conv9 = concatenate([conv9, conv2], axis=-1, name="seg_conc3")
    conv10 = block_layers(3, 128, conv9, name='seg4', max_pool=False,up_sample=True)
    conv10 = Dropout(0.2, name="seg_dropout2")(conv10)
    conv10 = concatenate([conv10, conv1], axis=-1, name="seg_conc4")
    conv11 = block_layers(3, 64, conv10, name='seg5', max_pool=False,up_sample=True)
    conv11 = concatenate([conv11, inputs], axis=-1, name="seg_conc5")

    segmentation_output = block_layers(2, 32, conv11, name='seg6', max_pool=False,up_sample=False)
    segmentation_output = Conv2D(filters=32, kernel_size=3, padding="same", activation="relu",name='segmentation_output')(segmentation_output)




    embedding_model = Model(inputs=[inputs], outputs=[detection_output,segmentation_output])
    return embedding_model


def CustomLossStep3(y_true, y_pred):
    # 1. Classification
    is_center = y_true[:, :, :, 0] == 1
    center_error = 30 * keras.losses.binary_crossentropy(tf.reshape(y_true[:, :, :, 0],
                                                                    [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]),
                                                         tf.reshape(y_pred[:, :, :, 0],
                                                                    [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]))
    non_center_error = keras.losses.binary_crossentropy(tf.reshape(y_true[:, :, :, 0],
                                                                   [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]),
                                                        tf.reshape(y_pred[:, :, :, 0],
                                                                   [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]))
    # Returns the right loss for each type of error.
    loss_classification = tf.where(is_center, center_error, non_center_error)

    # 2. correction- x/y
    los_corr_x_center = 30 * tf.keras.losses.MSE(tf.reshape(y_true[:, :, :, 1],
                                                            [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]),
                                                 tf.reshape(y_pred[:, :, :, 1],
                                                            [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]))
    loss_corr_y_center = 30 * tf.keras.losses.MSE(tf.reshape(y_true[:, :, :, 2],
                                                             [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]),
                                                  tf.reshape(y_pred[:, :, :, 2],
                                                             [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]))
    loss_regr_corr_x = tf.where(is_center, los_corr_x_center, 0)
    loss_regr_corr_y = tf.where(is_center, loss_corr_y_center, 0)

    # 3. bbox- width/height
    los_width = 30 * tf.keras.losses.MSE(tf.reshape(y_true[:, :, :, 3],
                                                    [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]),
                                         tf.reshape(y_pred[:, :, :, 3], [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]))
    loss_height = 30 * tf.keras.losses.MSE(tf.reshape(y_true[:, :, :, 4],
                                                      [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]),
                                           tf.reshape(y_pred[:, :, :, 4], [-1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 1]))
    loss_regr_width = tf.where(is_center, los_width, 0)
    loss_regr_height = tf.where(is_center, loss_height, 0)

    # 3. classification weight
    alpha = 0.4
    beta = 0.3

    loss = alpha * loss_classification + beta * (loss_regr_width + loss_regr_height) + \
           (1 - alpha - beta) * (loss_regr_corr_x + loss_regr_corr_y)

    return loss