from dataset import get_training_validation_generators, get_testing_generators
from models import CustomLossStep3, single_step_model_full
import tensorflow as tf
import datetime
from params import EPOCHS, LEARNING_RATE
from params import HEIGHT, WIDTH, LAST_LAYER_WIDTH, LAST_LAYER_HEIGHT, THRESHOLD
from numpy.random import seed
from utils import display_ssm_true_predicted_centers
seed(42)  # keras seed fixing import tensorflow as tf
tf.random.set_seed(42)  # tensorflow seed fixing


def main():
    # Generators - training and validation
    training_generator, validation_generator = get_training_validation_generators(0.9)
    # Generators - testing
    testing_generator = get_testing_generators()

    # model for centers detection
    ssm = single_step_model_full()
    print(ssm.summary())
    tf.keras.utils.plot_model(ssm, to_file="SingleStep Model step3.png", show_shapes=True)

    # model compile
    opt = tf.keras.optimizers.Adam(learning_rate=LEARNING_RATE)
    ssm.compile(optimizer=opt, loss=CustomLossStep3, metrics=['accuracy'])

    # callbacks
    checkpoint_callback = tf.keras.callbacks.ModelCheckpoint('models/model-train test.{epoch:02d}.h5', monitor='loss',
                                                             verbose=1, save_best_only=True, mode='min')
    checkpoint_callback_val = tf.keras.callbacks.ModelCheckpoint('models/model-validation test.{epoch:02d}.h5', monitor='val_loss',
                                                             verbose=1, save_best_only=True, mode='min')
    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
    early_stop_callback = tf.keras.callbacks.EarlyStopping(patience=10, monitor='loss') # lossval_loss
    reduce_lr_on_plateau_callback = tf.keras.callbacks.ReduceLROnPlateau(monitor="loss", factor=0.5, patience=10,
                                                                         verbose=1, mode="min", min_delta=0.0001,
                                                                         cooldown=0, min_lr=0)
    callbacks_list = [checkpoint_callback, tensorboard_callback, reduce_lr_on_plateau_callback, checkpoint_callback_val]

    # Train model on dataset
    ssm.fit(x=training_generator,
            validation_data=testing_generator,
            epochs=EPOCHS,
            callbacks=callbacks_list) # validation_data=validation_generator, steps_per_epoch = 500

    results = ssm.evaluate(testing_generator)
    print(f"results on testing: {results}")
    # results = ssm.evaluate(validation_generator)
    # print(f"results on validation: {results}")
    results = ssm.evaluate(training_generator)
    print(f"results on training: {results}")

    # Print specific x, y from batch
    # first example in batch
    batch_num = 0
    batch_example = 0

    X, y = training_generator.__getitem__(batch_num)
    x = X[batch_example].reshape((1, HEIGHT, WIDTH, 3))
    y = y[batch_example].reshape((1, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 5))
    results = ssm.evaluate(x, y)
    print(f"results: {results}")

    # centers
    prediction = ssm.predict(x)
    below_half_gt = y[:, :, :, 0] < THRESHOLD
    gt = tf.where(below_half_gt, 0, 1)
    print(f"Actual: \n{gt}")
    below_half = prediction[:, :, :, 0] < THRESHOLD
    predictions = tf.where(below_half, 0, 1)
    print(f"Predictions: \n{predictions}")

    # corrections
    corr_x = y[:, :, :, 1]
    corr_y = y[:, :, :, 2]
    print(f"Y : corr_x:\n{corr_x}\ncorr_y:\n{corr_y}")
    corr_x = prediction[:, :, :, 1]
    corr_y = prediction[:, :, :, 2]
    print(f"Prediction: corr_x:\n{corr_x}\ncorr_y:\n{corr_y}")

    # width/height
    width = y[:, :, :, 3]
    height = y[:, :, :, 4]
    print(f"Y : width:\n{width}\nheight:\n{height}")
    width = prediction[:, :, :, 3]
    height = prediction[:, :, :, 4]
    print(f"Prediction: width:\n{width}\nheight:\n{height}")

    # for display
    x_rs = x.reshape((HEIGHT, WIDTH, 3))
    y_rs = y.reshape((LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 5))
    pred_rs = tf.reshape(prediction, [LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 5]).numpy()
    display_ssm_true_predicted_centers(x_rs,
                                       y_rs,
                                       pred_rs)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

