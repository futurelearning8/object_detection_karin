from tensorflow import keras
from dataset import get_testing_generators, get_training_validation_generators
import tensorflow as tf
from models import CustomLossStep3, loss_return_0
from numpy.random import seed
from object_detection_utils import NMS_on_generator
from CONSTANTS import PATH_TO_RESULT, INSTANCES_TEST_PATH, PATH_TO_SEG_JSON, INSTANCES_TRAIN_PATH
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import numpy as np
from params import LEARNING_RATE, BATCH_SIZE, HEIGHT, WIDTH
from utils import display_image

seed(42)  # keras seed fixing import tensorflow as tf
tf.random.set_seed(42)  # tensorflow seed fixing


def evaluate_model():
    model_path = 'models/validation.09.h5'
    # model_path = 'models/0906-0028lr_0.0005batch_size_32/validation.06.h5'
    # Best Model Loading
    unet = keras.models.load_model(model_path, compile=False)

    # model compile
    opt = tf.keras.optimizers.Adam(learning_rate=LEARNING_RATE)
    losses = {
        "detection_output": CustomLossStep3,
        "segmentation_output": loss_return_0,
    }
    lossWeights = {"detection_output": 1.0, "segmentation_output": 1.0}
    unet.compile(optimizer=opt, loss=losses, metrics=["accuracy"])

    # Generators - training and validation
    training_generator, validation_generator = get_training_validation_generators(0.9)
    # Generators - testing
    testing_generator = get_testing_generators()

    # Results for test set
    #results = unet.evaluate(training_generator)
    #print(results)

    # https://github.com/cocodataset/cocoapi/blob/master/PythonAPI/pycocoEvalDemo.ipynb

    # for i in range(testing_generator.__len__()):
    #     X, masks = testing_generator.__getitem__(i)
    #     for j in range(BATCH_SIZE):
    #         x = X[j].reshape((1, HEIGHT, WIDTH, 3))
    #         mask = masks[j].reshape((1, HEIGHT, HEIGHT, 1))
    #         predicted_mask = ssm.predict(x)
    #         display_image(I=x.reshape((HEIGHT, WIDTH, 3)), mask=mask.reshape((HEIGHT, WIDTH)), predicted_mask=predicted_mask.reshape((HEIGHT, WIDTH)))


    annFile = INSTANCES_TEST_PATH
    # annFile = INSTANCES_TRAIN_PATH
    cocoGt = COCO(annFile)
    annType = ['segm', 'bbox', 'keypoints']
    annType = annType[1]

    MAP_MAR = []
    for th in np.arange(0.7, 0.9, 0.05):
        for nms in np.arange(0.5, 0.6, 0.05):
            lst_ids = NMS_on_generator(unet, testing_generator, th, nms, display=False)

            cocoDt = cocoGt.loadRes(PATH_TO_RESULT)
            coco_sap_dt = cocoGt.loadRes(PATH_TO_RESULT)
            cocoSg = cocoGt.loadRes(PATH_TO_SEG_JSON)
            # running evaluation
            cocoEval_dt = COCOeval(cocoGt, cocoDt, 'bbox')
            # imgIds = sorted(cocoGt.getImgIds())
            cocoEval_dt.params.imgIds = sorted(lst_ids)
            cocoEval_dt.params.catIds = [1]
            cocoEval_dt.params.areaRng = [[10000, 100000], [0 ** 2, 32 ** 2], [32 ** 2, 96 ** 2], [96 ** 2, 1e5 ** 2]] #[[0, 1e10], [0, 1e4], [1e4, 1e5], [1e5, 1e10]]
            cocoEval_dt.evaluate()
            cocoEval_dt.accumulate()
            cocoEval_dt.summarize()
            stats = cocoEval_dt.stats
            MAP_MAR.append((stats[4], stats[10]))
            print(f"Detection: th={th}, nmsTh={nms}")

            # running evaluation
            cocoEval_dt = COCOeval(cocoGt, coco_sap_dt, 'segm')
            # imgIds = sorted(cocoGt.getImgIds())
            cocoEval_dt.params.imgIds = sorted(lst_ids)
            cocoEval_dt.params.catIds = [1]
            cocoEval_dt.params.areaRng = [[10000, 100000], [0 ** 2, 32 ** 2], [32 ** 2, 96 ** 2], [96 ** 2, 1e5 ** 2]] #[[0, 1e10], [0, 1e4], [1e4, 1e5], [1e5, 1e10]]
            cocoEval_dt.evaluate()
            cocoEval_dt.accumulate()
            cocoEval_dt.summarize()
            stats = cocoEval_dt.stats
            MAP_MAR.append((stats[4], stats[10]))
            print(f"Separated segmentation: th={th}, nmsTh={nms}")

            cocoEval_dt = COCOeval(cocoGt, cocoSg, 'segm')
            # imgIds = sorted(cocoGt.getImgIds())
            cocoEval_dt.params.imgIds = sorted(lst_ids)
            cocoEval_dt.params.catIds = [1]
            cocoEval_dt.params.areaRng = [[10000, 100000], [0 ** 2, 32 ** 2], [32 ** 2, 96 ** 2], [96 ** 2, 1e5 ** 2]] #[[0, 1e10], [0, 1e4], [1e4, 1e5], [1e5, 1e10]]
            cocoEval_dt.evaluate()
            cocoEval_dt.accumulate()
            cocoEval_dt.summarize()
            stats = cocoEval_dt.stats
            MAP_MAR.append((stats[4], stats[10]))
            print(f"Segmentation: th={th}, nmsTh={nms}")


if __name__ == "__main__":
    evaluate_model()
