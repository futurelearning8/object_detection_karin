from dataset import get_training_validation_generators, get_testing_generators
from models import CustomLossStep3, unet_model
import tensorflow as tf
import datetime
from params import EPOCHS, LEARNING_RATE,LOAD,BATCH_SIZE,AUG
from numpy.random import seed
seed(42)  # keras seed fixing import tensorflow as tf
tf.random.set_seed(42)  # tensorflow seed fixing
from tensorflow import keras


def main():
    # Generators - training and validation
    training_generator, validation_generator = get_training_validation_generators(0.9)
    # Generators - testing
    testing_generator = get_testing_generators()

    if not LOAD:
        print("Creating a model...")
        # Model Creation
        # model for centers detection
        unet = unet_model()
        print(unet.summary())
        tf.keras.utils.plot_model(unet, to_file="Unet step1.png", show_shapes=True)

        # model compile
        opt = tf.keras.optimizers.Adam(learning_rate=LEARNING_RATE)
        unet.compile(optimizer=opt, loss="binary_crossentropy", metrics=['accuracy'])

    else:
        # Model Loading
        model_path = 'models/model-train.14.h5'
        print(f"Load model: {model_path}...")
        # Best Model Loading
        unet = keras.models.load_model(model_path, compile=False)

        # compile best model with relevant optimizer, loss and metrics
        opt = tf.keras.optimizers.Adam(learning_rate=LEARNING_RATE)

        # Bug with accuracy
        unet.compile(optimizer=opt, loss="binary_crossentropy", metrics=['accuracy'])

    # callbacks
    checkpoint_callback = tf.keras.callbacks.ModelCheckpoint('models/model-train.{epoch:02d}.h5', monitor='loss',
                                                             verbose=1, save_best_only=True, mode='min')
    checkpoint_callback_val = tf.keras.callbacks.ModelCheckpoint('models/model-validation.{epoch:02d}.h5', monitor='val_loss',
                                                             verbose=1, save_best_only=True, mode='min')
    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
    early_stop_callback = tf.keras.callbacks.EarlyStopping(patience=10, monitor='loss') # lossval_loss
    reduce_lr_on_plateau_callback = tf.keras.callbacks.ReduceLROnPlateau(monitor="loss", factor=0.5, patience=10,
                                                                         verbose=1, mode="min", min_delta=0.0001,
                                                                         cooldown=0, min_lr=0)
    callbacks_list = [checkpoint_callback, tensorboard_callback, reduce_lr_on_plateau_callback, checkpoint_callback_val]

    if not LOAD:
        # Train model on dataset
        unet.fit(x=training_generator,
                validation_data=validation_generator,
                epochs=EPOCHS,
                callbacks=callbacks_list,
                ) # validation_data=validation_generator, steps_per_epoch = 500, validation_steps=2
    else:
        unet.fit(x=training_generator,
                validation_data=validation_generator,
                epochs=EPOCHS,
                callbacks=callbacks_list,
                initial_epoch=14) # validation_data=validation_generator, steps_per_epoch = 500, validation_steps=2

    results = unet.evaluate(testing_generator)
    print(f"results on testing: {results}")
    results = unet.evaluate(validation_generator)
    print(f"results on validation: {results}")
    results = unet.evaluate(training_generator)
    print(f"results on training: {results}")


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

