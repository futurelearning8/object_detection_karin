from coco_anno_reader_fl import get_input_for_fl
from get_data_for_model import get_data_for_model
from CONSTANTS import INSTANCES_TRAIN_PATH, TESTING_PATH, TRAIN_PATH, INSTANCES_TEST_PATH
from utils import images_preprocessing
import numpy as np
from tensorflow import keras
from params import RESIZE_X, RESIZE_Y, BATCH_SIZE, HEIGHT, WIDTH, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, DATA_SIZE, AUG,SEG
from augmented_data_generator import AugmentDataGenerator
import albumentations as A


import random
import math
random.seed(42)
np.random.seed(42)


def get_training_validation_generators(training_ratio):
    # get images names + path from training path
    # data = get_input_for_fl(INSTANCES_TRAIN_PATH, min_area=10000, max_area=100000, category_id=1)
    data = get_data_for_model(INSTANCES_TRAIN_PATH, min_area=10000, max_area=100000)
    if DATA_SIZE is not None:
        data = data[0:DATA_SIZE]
    random.shuffle(data)
    training_size = int(training_ratio * len(data)) # len(data)
    image_bbox_lst_training = data[0:training_size]
    image_bbox_lst_validation = data[training_size:]

    # Parameters
    params = {'dim': (HEIGHT, WIDTH),
              'batch_size': BATCH_SIZE,
              'shuffle': True}

    # Generators
    if SEG:
        training_generator = DataGeneratorBBox(image_bbox_lst_training, mode="Training", **params)
        if AUG:
            training_generator = AugmentDataGenerator(training_generator, A.Compose([
                    A.Flip(),
                    A.Rotate(),
                    ],additional_targets={'y_true': 'image'}))

        validation_generator = DataGeneratorBBox(image_bbox_lst_validation, mode="Validation", **params)
    else:
        training_generator = DataGeneratorBBoxSegmentation(image_bbox_lst_training, mode="Training", **params)
        if AUG:
            training_generator = AugmentDataGenerator(training_generator, A.Compose([
                #todo add here augmentations of colors- hue crightness colorshift, weather
                    A.Blur(blur_limit=3, p=0.15),
                    A.RandomBrightnessContrast(p=0.15),
                    A.ToGray(p=0.1),
                    A.RandomGamma(p=0.15)


                ],))

        validation_generator = DataGeneratorBBoxSegmentation(image_bbox_lst_validation, mode="Validation", **params)

    return training_generator, validation_generator


def get_testing_generators():
    # get images names + path from training path
    # data = get_input_for_fl(INSTANCES_TEST_PATH, min_area=10000, max_area=100000, category_id=1)
    data = get_data_for_model(INSTANCES_TEST_PATH, min_area=10000, max_area=100000)
    random.shuffle(data)

    # Parameters
    params = {'dim': (HEIGHT, WIDTH),
              'batch_size': BATCH_SIZE,
              'shuffle': True}

    # Generators
    if SEG:
        training_generator = DataGeneratorBBox(data, mode="Testing", **params)
    else:
        training_generator = DataGeneratorBBoxSegmentation(data, mode="Testing", **params)
    return training_generator


class DataGeneratorBBox(keras.utils.Sequence):
    """Generates data for Keras"""
    def __init__(self, list_IDs, mode, batch_size=BATCH_SIZE, dim=(RESIZE_Y, RESIZE_X), shuffle=True):
        """Initialization"""
        self.dim = dim
        self.batch_size = batch_size
        self.list_IDs = list_IDs
        self.shuffle = shuffle
        self.mode = mode
        if mode in ["Training", "Validation"]:
            self.data_path = TRAIN_PATH
        else:
            self.data_path = TESTING_PATH

        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, bbox, masks, _, _ = self.__data_generation(list_IDs_temp)

        return X, masks

    def getitem_id(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, bbox, masks, ids, wh = self.__data_generation(list_IDs_temp)

        return X, bbox, masks, ids, wh

    def on_epoch_end(self):
        """Updates indexes after each epoch"""
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples'
        # Initialization
        y = np.zeros((self.batch_size, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 5))
        masks_y = np.zeros((self.batch_size, RESIZE_Y, RESIZE_X, 1))

        # labels, images_file_names = None, None
        images_file_names = [item[0] for item in list_IDs_temp]
        bboxes = [item[1] for item in list_IDs_temp]
        masks = [item[2] for item in list_IDs_temp]
        ids = np.array([item[3] for item in list_IDs_temp])
        width_height = [item[4] for item in list_IDs_temp]

        X, imgs_data_bboxes, imgs_data_masks = images_preprocessing(images_file_names, self.mode, bboxes, masks)
        X = np.array(X)
        width_label_scale = LAST_LAYER_WIDTH / WIDTH
        height_label_scale = LAST_LAYER_HEIGHT / HEIGHT

        for i, img_data in enumerate(imgs_data_bboxes):
            for center_data in img_data:
                correction_x, last_layer_x = math.modf(center_data[0] * width_label_scale)
                last_layer_x = int(last_layer_x)
                correction_y, last_layer_y = math.modf(center_data[1] * height_label_scale)
                last_layer_y = int(last_layer_y)
                y[i, last_layer_y, last_layer_x, 0] = 1
                # correction
                y[i, last_layer_y, last_layer_x, 1] = correction_x
                y[i, last_layer_y, last_layer_x, 2] = correction_y
                # scaled width and height
                y[i, last_layer_y, last_layer_x, 3] = center_data[2]
                y[i, last_layer_y, last_layer_x, 4] = center_data[3]

        for i, img_mask in enumerate(imgs_data_masks):
            masks_y[i] = img_mask.reshape((RESIZE_Y, RESIZE_X, 1))

        # y = mask
        return X, y, masks_y, ids, width_height


class DataGeneratorBBoxSegmentation(keras.utils.Sequence):
    """Generates data for Keras"""
    def __init__(self, list_IDs, mode, batch_size=BATCH_SIZE, dim=(RESIZE_Y, RESIZE_X), shuffle=True):
        """Initialization"""
        self.dim = dim
        self.batch_size = batch_size
        self.list_IDs = list_IDs
        self.shuffle = shuffle
        self.mode = mode
        if mode in ["Training", "Validation"]:
            self.data_path = TRAIN_PATH
        else:
            self.data_path = TESTING_PATH

        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, bbox, masks, _, _, _ = self.__data_generation(list_IDs_temp)

        return X, [bbox, masks]

    def getitem_id(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, bboxes, masks, ids, wh, instace_mask = self.__data_generation(list_IDs_temp)

        return X, bboxes, masks, ids, wh, instace_mask

    def on_epoch_end(self):
        """Updates indexes after each epoch"""
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples'
        # Initialization
        y = np.zeros((self.batch_size, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 5))
        masks_y = np.zeros((self.batch_size, RESIZE_Y, RESIZE_X, 1))
        instance_masks_y = np.zeros((self.batch_size, RESIZE_Y, RESIZE_X, 1))

        # labels, images_file_names = None, None
        images_file_names = [item[0] for item in list_IDs_temp]
        bboxes = [item[1] for item in list_IDs_temp]
        masks = [item[2] for item in list_IDs_temp]
        ids = np.array([item[3] for item in list_IDs_temp])
        width_height = [item[4] for item in list_IDs_temp]
        instance_masks = [item[5] for item in list_IDs_temp]

        X, imgs_data_bboxes, imgs_data_masks, imgs_data_instance_masks = images_preprocessing(images_file_names,
                                                                                              self.mode, bboxes, masks,
                                                                                              instance_masks)
        X = np.array(X)
        width_label_scale = LAST_LAYER_WIDTH / WIDTH
        height_label_scale = LAST_LAYER_HEIGHT / HEIGHT

        for i, img_data in enumerate(imgs_data_bboxes):
            for center_data in img_data:
                correction_x, last_layer_x = math.modf(center_data[0] * width_label_scale)
                last_layer_x = int(last_layer_x)
                correction_y, last_layer_y = math.modf(center_data[1] * height_label_scale)
                last_layer_y = int(last_layer_y)
                y[i, last_layer_y, last_layer_x, 0] = 1
                # correction
                y[i, last_layer_y, last_layer_x, 1] = correction_x
                y[i, last_layer_y, last_layer_x, 2] = correction_y
                # scaled width and height
                y[i, last_layer_y, last_layer_x, 3] = center_data[2]
                y[i, last_layer_y, last_layer_x, 4] = center_data[3]

        for i, img_mask in enumerate(imgs_data_masks):
            masks_y[i] = img_mask.reshape((RESIZE_Y, RESIZE_X, 1))

        for i, img_mask in enumerate(imgs_data_instance_masks):
            instance_masks_y[i] = img_mask.reshape((RESIZE_Y, RESIZE_X, 1))

        # y = mask
        return X, y, masks_y, ids, width_height, instance_masks_y
