from params import RESIZE_X, RESIZE_Y, HEIGHT, WIDTH, BATCH_SIZE, LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH
import cv2
import numpy as np
from matplotlib.collections import PatchCollection
from matplotlib.patches import Polygon, Circle
import matplotlib.pyplot as plt
import skimage.io as io
from CONSTANTS import TRAIN_PATH, TESTING_PATH
import tensorflow as tf
from copy import copy


def display_image(I=None, image_path=None, bboxes=None, predicted_bboxes=None, centers=None, predicted_centers=None,
                  mask=None, predicted_mask=None, predicted_instance_mask=None):
    polygons1 = []
    polygons2 = []
    predicted_polygons1 = []
    predicted_polygons2 = []
    circles1 = []
    circles2 = []
    color = []
    predicted_circles1 = []
    predicted_circles2 = []

    fig1 = plt.figure()
    ax0 = fig1.add_subplot(151)
    ax1 = fig1.add_subplot(152)
    ax2 = fig1.add_subplot(153)
    ax3 = fig1.add_subplot(154)
    ax4 = fig1.add_subplot(155)
    ax0.title.set_text('Image')
    ax1.title.set_text('Both')
    ax2.title.set_text('Ground Truth')
    ax3.title.set_text('Predictions')
    ax4.title.set_text('Instance')

    # ax = plt.gca()
    if bboxes is not None:
        for bbox in bboxes:
            c = (np.random.random((1, 3)) * 0.6 + 0.4).tolist()[0]
            color.append(c)

            [bbox_x, bbox_y, bbox_w, bbox_h] = bbox
            poly = [[bbox_x, bbox_y], [bbox_x, bbox_y + bbox_h], [bbox_x + bbox_w, bbox_y + bbox_h],
                    [bbox_x + bbox_w, bbox_y]]
            np_poly = np.array(poly).reshape((4, 2))
            polygons1.append(Polygon(np_poly, facecolor=None, fill=False))
            polygons2.append(Polygon(np_poly, facecolor=None, fill=False))

        p1 = PatchCollection(polygons1, linewidths=5, alpha=0.2, edgecolors='blue', linestyle = '--')
        p2 = PatchCollection(polygons2, linewidths=5, alpha=0.2, edgecolors='blue', linestyle = '--')
        ax1.add_collection(p1)
        ax2.add_collection(p2)

    if predicted_bboxes is not None:
        color = []
        for bbox in predicted_bboxes:
            c = (np.random.random((1, 3)) * 0.6 + 0.4).tolist()[0]
            color.append(c)

            [bbox_x, bbox_y, bbox_w, bbox_h] = bbox
            poly = [[bbox_x, bbox_y], [bbox_x, bbox_y + bbox_h], [bbox_x + bbox_w, bbox_y + bbox_h],
                    [bbox_x + bbox_w, bbox_y]]
            np_poly = np.array(poly).reshape((4, 2))
            predicted_polygons1.append(Polygon(np_poly, facecolor=None, fill=False))
            predicted_polygons2.append(Polygon(np_poly, facecolor=None, fill=False))

        p1 = PatchCollection(predicted_polygons1, linewidths=5, alpha=0.4, edgecolors='red', linestyle='-.')
        p2 = PatchCollection(predicted_polygons2, linewidths=5, alpha=0.4, edgecolors='red', linestyle='-.')
        ax1.add_collection(p1)
        ax3.add_collection(p2)

    if centers is not None:
        color = []
        for center in centers:
            c = (np.random.random((1, 3)) * 0.6 + 0.4).tolist()[0]
            color.append(c)

            [center_x, center_y] = center
            circle = [(center_x, center_y), 3]
            circles1.append(Circle(*circle))
            circles2.append(Circle(*circle))

        p1 = PatchCollection(circles1, edgecolors='blue', facecolor=color, linewidths=3, alpha=1)
        p2 = PatchCollection(circles2, edgecolors='blue', facecolor=color, linewidths=3, alpha=1)
        ax1.add_collection(p1)
        ax2.add_collection(p2)

    if predicted_centers is not None:
        color = []
        for center in predicted_centers:
            c = (np.random.random((1, 3)) * 0.6 + 0.4).tolist()[0]
            color.append(c)

            [center_x, center_y] = center
            circle = [(center_x, center_y), 3]
            predicted_circles1.append(Circle(*circle))
            predicted_circles2.append(Circle(*circle))

        p1 = PatchCollection(predicted_circles1, edgecolors='red', facecolor=color, linewidths=2, alpha=0.5)
        p2 = PatchCollection(predicted_circles2, edgecolors='red', facecolor=color, linewidths=2, alpha=0.5)
        ax1.add_collection(p1)
        ax3.add_collection(p2)

    if image_path is not None:
        I = io.imread(image_path)
    ax0.imshow(I)
    ax1.imshow(I)
    ax2.imshow(I)
    ax3.imshow(I)
    ax4.imshow(I)
    if mask is not None:
        # mask[mask == 1] = 0.8
        ax1.imshow(mask, alpha=0.5)
        ax2.imshow(mask, alpha=0.8)
    if predicted_mask is not None:
        # predicted_mask[predicted_mask > 0.5] = 0.8
        # predicted_mask[predicted_mask <= 0.5] = 0
        ax1.imshow(predicted_mask, alpha=0.5)
        ax3.imshow(predicted_mask, alpha=0.8)
        ax4.imshow(predicted_instance_mask, alpha=0.8)
    plt.show()


def images_preprocessing(images_file_names, mode, bboxes=None, masks=None, instance_masks=None):
    """Return images after preprocessing: resize + centers"""
    if mode in ["Training", "Validation"]:
        data_path = TRAIN_PATH
    else:
        data_path = TESTING_PATH

    X = []
    processed_bboxes = []
    processed_masks = []
    processed_instance_masks = []

    if masks is not None:
        for image_file_name, bbox, mask, instance_mask in zip(images_file_names, bboxes, masks, instance_masks):
            resized_img, centers, resized_mask, resized_instance_mask = image_centers_and_bbox_preprocessing(
                image_path=data_path+image_file_name, bboxes=bbox, mask=mask, instance_mask=instance_mask)
            X.append(resized_img)
            processed_bboxes.append(centers)
            processed_masks.append(resized_mask)
            processed_instance_masks.append(resized_instance_mask)
            # y.append(centers)
    else:
        for image_file_name in images_file_names:
            resized_img, _, _, _ = image_centers_and_bbox_preprocessing(image_path=data_path+image_file_name)
            X.append(resized_img)

    return X, processed_bboxes, processed_masks, processed_instance_masks


def image_centers_and_bbox_preprocessing(image=None, image_path=None, bboxes=None, mask=None, instance_mask=None):
    # resize image
    resized_img, resized_mask, resized_instance_mask, x_scale, y_scale = \
        image_preprocess(image, image_path, mask, instance_mask)

    # resize centers
    centers_and_bboxes = []
    for bbox in bboxes:
        [bbox_x, bbox_y, bbox_w, bbox_h] = bbox
        center_x_scale = bbox_x * x_scale + (bbox_w * x_scale) / 2
        center_y_scale = bbox_y * y_scale + (bbox_h * y_scale) / 2
        bbox_w_norm = bbox_w * x_scale / RESIZE_X
        bbox_h_norm = bbox_h * y_scale / RESIZE_Y

        centers_and_bboxes.append([center_x_scale, center_y_scale, bbox_w_norm, bbox_h_norm])

    # centers = [(item[0], item[1]) for item in centers_and_bboxes]
    # display_image(I=resized_img, image_path=None, centers=centers)
    return resized_img, centers_and_bboxes, resized_mask, resized_instance_mask


def image_preprocess(image, image_path, mask=None, instance_mask=None):
    """Image resizing + scale"""
    resized_mask = None
    resized_instance_mask = None
    if image_path is not None:
        image = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = image / 255.0
    original_height = image.shape[0]
    original_width = image.shape[1]
    x_scale = RESIZE_X / original_width
    y_scale = RESIZE_Y / original_height
    resized_img = cv2.resize(image, (RESIZE_Y, RESIZE_X))
    if mask is not None:
        resized_mask = cv2.resize(mask.astype('float32'), (RESIZE_Y, RESIZE_X))
        resized_instance_mask = cv2.resize(instance_mask.astype('float32'), (RESIZE_Y, RESIZE_X))

    return resized_img, resized_mask, resized_instance_mask, x_scale, y_scale


def display_ssm_true_predicted_centers(image, true_centers, predicted_centers):
    # centers last layer indices
    img_true_centers_indexed = lambda: zip(*np.where(true_centers[:, :, 0] == 1))
    img_predicted_centers_indexed = lambda: zip(*np.where(predicted_centers[:, :, 0] > 0.5))

    # indices in resized image
    width_label_scale = WIDTH / LAST_LAYER_WIDTH
    height_label_scale = HEIGHT / LAST_LAYER_HEIGHT

    img_true_bboxes = [(max((x + true_centers[y, x, 1]) * width_label_scale - true_centers[y, x, 3] / 2 * WIDTH, 0),
                        max((y + true_centers[y, x, 2]) * height_label_scale - true_centers[y, x, 4] / 2 * HEIGHT, 0),
                        true_centers[y, x, 3] * WIDTH,
                        true_centers[y, x, 4] * HEIGHT)
                       for (y, x) in img_true_centers_indexed()]
    img_predicted_bboxes = [(max((x + predicted_centers[y, x, 1]) * width_label_scale - predicted_centers[y, x, 3] / 2 * WIDTH, 0),
                        max((y + predicted_centers[y, x, 2]) * height_label_scale - predicted_centers[y, x, 4] / 2 * HEIGHT, 0),
                        predicted_centers[y, x, 3] * WIDTH,
                        predicted_centers[y, x, 4] * HEIGHT)
                       for (y, x) in img_predicted_centers_indexed()]
    img_true_centers_indexed = [((x + true_centers[y, x, 1]) * width_label_scale,
                                 (y + true_centers[y, x, 2]) * height_label_scale) for (y, x)
                                in img_true_centers_indexed()]
    img_predicted_centers_indexed = [((x + predicted_centers[y, x, 1]) * width_label_scale,
                                      (y + predicted_centers[y, x, 2]) * height_label_scale) for (y, x)
                                     in img_predicted_centers_indexed()]
    display_image(I=image, centers=img_true_centers_indexed, predicted_centers=img_predicted_centers_indexed,
                  bboxes=img_true_bboxes, predicted_bboxes=img_predicted_bboxes)
    return img_true_bboxes, img_predicted_bboxes, img_true_centers_indexed, img_predicted_centers_indexed


def show_image_gt_pred(x, y, prediction):
    below_half_gt = y[:, :, :, 0] < 0.5
    gt = tf.where(below_half_gt, 0, 1)
    print(f"Actual: \n{gt}")
    below_half = prediction[:, :, :, 0] < 0.5
    predictions = tf.where(below_half, 0, 1)
    print(f"Predictions: \n{predictions}")
    # print(CustomBinaryCrossEntropy(y, prediction))

    corr_x = y[:, :, :, 1]
    corr_y = y[:, :, :, 2]
    print(f"Y : corr_x:\n{corr_x}\ncorr_y:\n{corr_y}")
    corr_x = prediction[:, :, :, 1]
    corr_y = prediction[:, :, :, 2]
    print(f"Prediction: corr_x:\n{corr_x}\ncorr_y:\n{corr_y}")

    width = y[:, :, :, 3]
    height = y[:, :, :, 4]
    print(f"Y : width:\n{width}\nheight:\n{height}")
    width = prediction[:, :, :, 3]
    height = prediction[:, :, :, 4]
    print(f"Prediction: width:\n{width}\nheight:\n{height}")

    x_rs = x.reshape((HEIGHT, WIDTH, 3))
    y_rs = y.reshape((LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 5))
    pred_rs = tf.reshape(prediction, [LAST_LAYER_HEIGHT, LAST_LAYER_WIDTH, 5]).numpy()
    img_true_bboxes, img_predicted_bboxes, img_true_centers_indexed, img_predicted_centers_indexed = \
        display_ssm_true_predicted_centers(x_rs, y_rs, pred_rs)
    return img_true_bboxes, img_predicted_bboxes, img_true_centers_indexed, img_predicted_centers_indexed
